package org.spview.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
 * Class to show a string array
 */

/////////////////////////////////////////////////////////////////////

/**
 * Window to show a string array.
 */
public class JFText extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/////////////////////////////////////////////////////////////////////

	public JFText(ArrayList<String> list, int cp_x, int cp_y) {

		super("List of points"); // main window
		setResizable(false);
		setAlwaysOnTop(true);

		addWindowListener(new WindowAdapter() { // clean end
			public void windowClosing(WindowEvent e) {
				dispose(); // free window
			}
		});
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		getContentPane().setLayout(new BorderLayout());
		JPanel jp = new JPanel(new BorderLayout());
		getContentPane().add(jp, "Center");

		// panels
		// panels
		JPanel pcentre = new JPanel();

		// center
		JTextArea jta = new JTextArea();
		int ml = 0; // max length of strings
		for (String s : list) {
			if (s.length() > ml) {
				ml = s.length();
			}
		}
		jta.setColumns(ml); // jta width
		for (String s : list) {
			jta.append(s); // add strings
		}
		jta.setBackground(Color.BLACK);
		jta.setForeground(Color.WHITE);
		jta.setEditable(false); // not editable
		jta.setFont(new Font("Monospaced", Font.PLAIN, 15));
		jta.setLineWrap(false); // no line wrap
		jta.setRows(10); // nb of lines
		// to show the string array
		// with lifts
		JScrollPane jsp = new JScrollPane(jta, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		try {
			jta.setCaretPosition(0); // at the top of the text
		} catch (IllegalArgumentException ignored) {
		}
		pcentre.add(jsp);

		// insert panels
		jp.add(pcentre, "Center");

		setLocation(cp_x, cp_y);
		pack();
	}

}