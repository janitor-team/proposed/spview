package org.spview.gui;

import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;

import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import org.spview.Spview;

public class About {

    static String versionnedName() {
        return ("About " + Spview.PACKAGE_NAME + " " + Spview.VERSION);
    }

    public static void show() {
        JEditorPane editorPane = new JEditorPane();
        editorPane.setContentType("text/html");
        editorPane.setEditable(false);
        URL helpURL = About.class.getResource("/about.html");

        if (helpURL != null) {
            try {
                editorPane.setPage(helpURL);
            } catch (IOException e) {
                System.err.println("Attempted to read a bad URL: " + helpURL);
            }
        } else {
            System.err.println("Couldn't find file: About.html");
            return;
        }
        final ImageIcon icon = new ImageIcon(new ImageIcon(Objects.requireNonNull(JobPlay.class.getResource("/pixmaps/spview.png"))).getImage()
                .getScaledInstance(64, 64, Image.SCALE_SMOOTH));
        // Put the editor pane in a scroll pane.
        JScrollPane scrollPane = new JScrollPane(editorPane);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(800, 600));
        JOptionPane.showMessageDialog(null, scrollPane, versionnedName(), JOptionPane.PLAIN_MESSAGE, icon);
    }
}
