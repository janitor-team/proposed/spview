package org.spview.point;
/**
 * This class allows sorting prediction jsyn data.
 */
public class PredJsynPoint implements Comparable<Object> {

    private final String jsyn;                                             // assignment
    private final int    ind;                                              // index before sort

    /**
     * Construct a new PredJsynPoint.
     *
     * @param cjsyn  assignment
     * @param cind   index before sort
     */
    public PredJsynPoint(String cjsyn, int cind) {

        jsyn = cjsyn;                                                // assignment
        ind  = cind;                                                 // index before sort
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get prediction assignment string.
     */
    public String getJsyn() {

        return jsyn;
    }

    /**
     * Get index before sort.
     */
    public int getInd() {

        return ind;
    }

    /**
     * Compare following assignment string.
     *
     * @param cpjp the PredJsynPoint to compare to
     */
    public int compareTo(Object cpjp) {

        if( ! (cpjp instanceof PredJsynPoint) ) {                    // not the right object
            throw new ClassCastException();
        }
        int delta = ((PredJsynPoint)cpjp).getJsyn().compareTo(jsyn);  // compare assignment strings
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        return 0;                                                    // equal
    }

}
