package org.spview.point;
/**
 * This class allows sorting stick file data.
 */
public class StickPoint implements Comparable<Object> {

    private final double x;                                                // frequency
    private final double y;                                                // intensity

    /**
     * Construct a new StickPoint.
     *
     * @param cx  frequency
     * @param cy  intensity
     */
    public StickPoint(double cx, double cy) {

        x = cx;                                                      // frequency
        y = cy;                                                      // intensity
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get frequency.
     */
    public double getX() {

        return x;
    }

    /**
     * Get intensity.
     */
    public double getY() {

        return y;
    }

    /**
     * Compare frequencies only.
     *
     * @param csp the StickPoint to compare to
     */
    public int compareTo(Object csp) {

        if( ! (csp instanceof StickPoint) ) {                        // not the right object
            throw new ClassCastException();
        }
        double delta = ((StickPoint)csp).getX() - x;                 // compare frequencies
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        return 0;                                                    // equal
    }

}
