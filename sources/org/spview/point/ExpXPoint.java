package org.spview.point;
/**
 * This class allows sorting experimental data following frequency order.
 */

public class ExpXPoint implements Comparable<Object> {

    private final String nuo;                                              // line number string
    private final double x;                                                // frequency
    private final String faso;                                             // frequency mark
    private final double y;                                                // intensity
    private final String saso;                                             // intensity mark
    private final String sdobs;                                            // frequency and intensity standard deviations
    private final String jsyn;                                             // assignment
    private final String exasg;                                            // EXASG selection string
    private final String comm;                                             // comment

    private int    inuo;                                             // line number int
    private double sdfreq;                                           // frequency standard deviation
    private double sdint;                                            // intensity standard deviation
    /**
     * Construct a new ExpXPoint.
     *
     * @param cnuo    line number
     * @param cx      frequency
     * @param cfaso   frequency mark
     * @param cy      intensity
     * @param csaso   intensity mark
     * @param csdobs  frequency and intensity standard deviations
     * @param cjsyn   assignment
     * @param cexasg  EXASG selection string
     * @param ccomm   comment
     */
    public ExpXPoint(String cnuo, double cx, String cfaso, double cy, String csaso,
                     String csdobs, String cjsyn, String cexasg, String ccomm) {

        nuo   = cnuo;                                                // line number
        x     = cx;                                                  // frequency
        faso  = cfaso;                                               // frequency mark
        y     = cy;                                                  // intensity
        saso  = csaso;                                               // intensity mark
        sdobs = csdobs;                                              // frequency and intensity standard deviations
        jsyn  = cjsyn;                                               // assignment
        exasg = cexasg;                                              // EXASG selection string
        comm  = ccomm;                                               // comment

        try {
            inuo = Integer.parseInt(nuo.trim());
        }
        catch (NumberFormatException e) {                            // format error
        }                                                            // already tested

        if( sdobs.length() == 0 ) {
            sdfreq = 0.;
            sdint  = 0.;
        }
        else {
            try {
                sdfreq = Double.parseDouble(sdobs.substring(0,10));
            }
            catch (NumberFormatException ignored) {
            }                                                        // already tested
            try {
                sdint  = Double.parseDouble(sdobs.substring(10,16));
            }
            catch (NumberFormatException ignored) {
            }                                                        // already tested
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get line number.
     */
    public String getNuo() {

        return nuo;
    }

    /*
     * Get line number (int).
     */
    public int getInuo() {

        return inuo;
    }

    /**
     * Get frequency.
     */
    public double getX() {

        return x;
    }

    /**
     * Get frequency mark.
     */
    public String getFaso() {

        return faso;
    }

    /**
     * Get intensity.
     */
    public double getY() {

        return y;
    }

    /**
     * Get intensity mark.
     */
    public String getSaso() {

        return saso;
    }

    /**
     * Get freqency and intensity standard deviations.
     */
    public String getSdobs() {

        return sdobs;
    }

    /*
     * Get frequency standard deviation.
     */
    public double getSdfreq() {

        return sdfreq;
    }

    /*
     * Get intensity standard deviation.
     */
    public double getSdint() {

        return sdint;
    }

    /**
     * Get assignment.
     */
    public String getJsyn() {

        return jsyn;
    }

    /**
     * Get EXASG selection string.
     */
    public String getExasg() {

        return exasg;
    }

    /**
     * Get comment.
     */
    public String getComm() {

        return comm;
    }

    /**
     * Compare according to x, nuo, y, sdfreq then sdint
     *
     * @param cexpt the ExpXPoint to compare to
     */
    public int compareTo(Object cexpt) {

        if( ! (cexpt instanceof ExpXPoint) ) {                       // not the right object
            throw new ClassCastException();
        }
        double xc      = ((ExpXPoint)cexpt).getX();
        int    inuoc   = ((ExpXPoint)cexpt).getInuo();
        double yc      = ((ExpXPoint)cexpt).getY();
        double sdfreqc = ((ExpXPoint)cexpt).getSdfreq();
        double sdintc  = ((ExpXPoint)cexpt).getSdint();
        int    idelta;
        double delta;

        delta = xc - x;                                               // frequency
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        else {
            idelta = inuoc - inuo;                                   // line number
            if     ( idelta < 0 ) {
                return  1;
            }
            else if( idelta > 0 ) {
                return -1;
            }
            else {
                delta = yc - y;                                      // intensity
                if     ( delta < 0 ) {
                    return  1;
                }
                else if( delta > 0 ) {
                    return -1;
                }
                else {
                    delta = sdfreqc - sdfreq;                        // frequency standard deviation
                    if     ( delta < 0 ) {
                        return  1;
                    }
                    else if( delta > 0 ) {
                        return -1;
                    }
                    else {
                        delta = sdintc - sdint;                      // intensity standard deviation
                        if     ( delta < 0 ) {
                            return  1;
                        }
                        else if( delta > 0 ) {
                            return -1;
                        }
                    }
                }
            }
        }
        return 0;                                                    // equal
    }

}
