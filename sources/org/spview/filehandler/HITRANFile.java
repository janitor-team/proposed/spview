package org.spview.filehandler;

import java.awt.geom.Point2D;

public class HITRANFile {

	public static Point2D extractXY(String str) {
		if (str.length() < 100) { // line too short
			return null; // skip it
		}
		// HITRAN pred-as-stick (100 for HITRAN, 160 for HITRAN 2004)
		double cx;
		double cy;
		try {
			// read data
			// extraction and validity test on 67 char (ie not all)
			Integer.parseInt(str.substring(0, 3).trim()); // validity test

			cx = Double.parseDouble(str.substring(3, 15)); // Double for X
			cy = Double.parseDouble(str.substring(15, 25)); // Double for Y

			Double.valueOf(str.substring(25, 35)); // validity test
			Double.valueOf(str.substring(35, 40)); // validity test
			Double.valueOf(str.substring(40, 45)); // validity test
			Double.valueOf(str.substring(45, 55)); // validity test
			Double.valueOf(str.substring(55, 59)); // validity test
			Double.valueOf(str.substring(59, 67)); // validity test
		} catch (NumberFormatException e) { // format error
			return null;
		}
		return (new Point2D.Double(cx, cy));
	}

	public static String extractAssignment(String str) {
		String cjsyn = "";
		if (str.length() == 100) {
			// HITRAN 2000
			cjsyn = str.substring(82, 85) + " " + str.substring(85, 87) + " " + str.substring(87, 90) + " "
					+ str.substring(73, 76) + " " + str.substring(76, 78) + " " + str.substring(78, 81);
		} else if (str.length() == 160) {
			// HITRAN 2004
			cjsyn = str.substring(112, 122) + " " + str.substring(97, 107);
		}
		return cjsyn;
	}

	/*
	 * get current extended assignment
	 * 
	 */
	public static String extractExtendedAssignment(String str) {
		String cxjsyn = "";
		if (str.length() == 100) {
			// HITRAN 2000
			cxjsyn = str.substring(67, 91);
		} else if (str.length() == 160) {
			// HITRAN 2004
			cxjsyn = str.substring(67, 127);
		}
		return cxjsyn;
	}
}